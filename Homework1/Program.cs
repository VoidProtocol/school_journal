﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool doContinue = true;
            ClassRegister Dziennik = null;

            Console.WriteLine("Witaj w SUPER DZIENNIK KREATOR™ autorstwa Michała Orchowskiego. Co chcesz dziś zrobić?");

            do
            {
                Console.WriteLine("\n1.Stwórz dziennik\n2.Dodaj dzień\n3.Dodaj pracę domową\n4.Raport\n5.Koniec");
                string action = Console.ReadLine();

                switch (action)
                {
                    case "1":
                        Dziennik = ClassRegister.CreateClassRegister();
                        break;
                    case "2":
                        Dziennik.CourseDay();
                        break;
                    case "3":
                        Dziennik.Homework();
                        break;
                    case "4":
                        Dziennik.Raport();
                        break;
                    case "5":
                        doContinue = false;
                        break;
                }
            } while (doContinue);
        }
    }
}
