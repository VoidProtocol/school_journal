﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    class ClassRegister
    {
        public string CourseName { get; set; }
        public string TeacherName { get; set; }
        public string CourseStartDate { get; set; }
        public double GradesTreshhold { get; set; }
        public double PresenceTreshhold { get; set; }
        public int NumberOfStudents { get; set; }
        public double MaxGradeScore { get; set; }
        public int MaxPresenceScore { get; set; }

        Dictionary<int, Student> Students;

        public ClassRegister()
        {

        }

        public ClassRegister(string courseName, string teacherName, string courseStartDate, double gradesTreshhold,
            double presenceTreshhold, int numberOfStudents, Dictionary<int, Student> students)
        {
            CourseName = courseName;
            TeacherName = teacherName;
            CourseStartDate = courseStartDate;
            GradesTreshhold = gradesTreshhold;
            PresenceTreshhold = presenceTreshhold;
            NumberOfStudents = numberOfStudents;
            Students = students;
        }

        public static ClassRegister CreateClassRegister()
        {
            Console.Write("Podaj nazwę kursu: ");
            string courseName = Console.ReadLine();
            Console.Write("Podaj imie i nazwisko prowadzącego: ");
            string teacherName = Console.ReadLine();
            Console.Write("Podaj datę rozpoczęcia kursu: ");
            string courseStartDate = Console.ReadLine();
            Console.Write("Podaj próg procentowy dla prac domowych: ");
            double gradesTreshhold = Convert.ToDouble(Console.ReadLine());
            Console.Write("Podaj próg procentowy dla obecności: ");
            double presenceTreshhold = Convert.ToDouble(Console.ReadLine());
            Console.Write("Podaj liczbę kursantów: ");
            int numberOfStudents = Convert.ToInt32(Console.ReadLine());
            Dictionary<int, Student> students = Student.CreateAllStudents(numberOfStudents);

            return new ClassRegister(courseName, teacherName, courseStartDate, gradesTreshhold,
                presenceTreshhold, numberOfStudents, students);
        }

        public void CourseDay()
        {
            string isPresent;

            MaxPresenceScore++;

            foreach (KeyValuePair<int, Student> student in Students)
            {
                Console.WriteLine($"Czy {student.Value.StudentFirstName} {student.Value.StudentSecondName} jest dziś " +
                    $"obecny? (obecny/nieobecny)");
                isPresent = Console.ReadLine();
                if (isPresent == "obecny")
                {
                    student.Value.PresenceScore++;
                }
            }

        }

        public void Homework()
        {
            Console.Write("Maksymalna Ocena za tą pracę domową: ");
            int MaxHomeworkGrade = Convert.ToInt32(Console.ReadLine());

            MaxGradeScore += MaxHomeworkGrade;

            foreach (KeyValuePair<int, Student> student in Students)
            {
                Console.WriteLine($"Jaką ocenę uzyskał {student.Value.StudentFirstName} {student.Value.StudentSecondName}?");
                student.Value.GradeScore += Convert.ToInt32(Console.ReadLine());
            }

        }

        public double PrecentOf(double firstNumber, double secondNumber)
        {
            double result;
            return result = firstNumber / secondNumber * 100;
        }

        public string PassCheck(double result, double treshhold)
        {
            if (result >= treshhold)
            {
                return "Zaliczone";
            }
            else
            {
                return "Niezaliczone";
            }
        }

        public void Raport()
        {
            Console.WriteLine($"Nazwa kursu: {CourseName}");
            Console.WriteLine($"Imie i nazwisko prowadzącego: {TeacherName}");
            Console.WriteLine($"Data rozpoczęcia kursu: {CourseStartDate}");
            Console.WriteLine($"Próg procentowy dla prac domowych: {GradesTreshhold}");
            Console.WriteLine($"Próg procentowy dla obecności: {PresenceTreshhold}");

            Console.WriteLine($"Ilość punktów zdobytych za obecność");

            foreach (KeyValuePair<int, Student> student in Students)
            {
                Console.WriteLine($"{student.Value.StudentFirstName} {student.Value.StudentSecondName} " +
                    $"({student.Value.PresenceScore}/{MaxPresenceScore} - " +
                    $"({PrecentOf(student.Value.PresenceScore, MaxPresenceScore)}%) - " +
                    $"{PassCheck(PrecentOf(student.Value.PresenceScore, MaxPresenceScore), PresenceTreshhold)}");
            }

            Console.WriteLine($"Ilość punktów zdobytych z prac domowych");

            foreach (KeyValuePair<int, Student> student in Students)
            {
                Console.WriteLine($"{student.Value.StudentFirstName} {student.Value.StudentSecondName} " +
                    $"({student.Value.GradeScore}/{MaxGradeScore} - " +
                    $"({PrecentOf(student.Value.GradeScore, MaxGradeScore):F0}%) - " +
                    $"{PassCheck(PrecentOf(student.Value.GradeScore, MaxGradeScore), GradesTreshhold)}");
            }
        }

    }
}
