﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    class Student
    {
        public int Pesel { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentSecondName { get; set; }
        public string StudentBirthdate { get; set; }
        public string StudentGender { get; set; }
        public double GradeScore { get; set; }
        public int PresenceScore { get; set; }

        public Student(int pesel, string studentFirstName, string studentSecondName, string studentBirthdate, 
            string studentGender)
        {
            Pesel = pesel;
            StudentFirstName = studentFirstName;
            StudentSecondName = studentSecondName;
            StudentBirthdate = studentBirthdate;
            StudentGender = studentGender;
        }

        public static Dictionary<int, Student> CreateAllStudents(int numberOfStudents)
        {
            Dictionary<int, Student> Students = new Dictionary<int, Student>();

            for (int i = 0; i < numberOfStudents; i++)
            {
                Console.Write("Podaj PESEL studenta: ");
                int pesel = Convert.ToInt32(Console.ReadLine());
                Console.Write("Podaj imie studenta: ");
                string studentFirstName = Console.ReadLine();
                Console.Write("Podaj nazwisko studenta: ");
                string studentSecondName = Console.ReadLine();
                Console.Write("Podaj datę urodzenia studenta: ");
                string studentBirthdate = Console.ReadLine();
                Console.Write("Podaj płeć studenta: ");
                string studentGender = Console.ReadLine();

                Students.Add(pesel, new Student(pesel, studentFirstName, studentSecondName, studentBirthdate, studentGender));
            }

            return Students;
        }

    }
}
